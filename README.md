# Audio Transcriber

Open in the TextGridEditor, one by one, a set of audio files along with their TextGrids. If a TextGrid is not found, it creates it.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

## Ideas for next realises

- Find only those files which are new or those which already exists
- Use recursive search
- Use relative paths
- Increase or decrese the volumne

### Prerequisites

What things you need to install the software and how to install them

```
Give examples
```

### Installing

Once installed, go to `Praat > Goodies`, you will see a new *menu command* named `View & Edit files from...`


If your files are too long, consider using the option `Open with LongSound`.


A step by step series of examples that tell you have to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Author

* **Rolando Muñoz** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth)

## License

This project is licensed under the GPL3 - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration
* etc
