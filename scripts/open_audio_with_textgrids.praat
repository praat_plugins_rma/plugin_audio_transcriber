# Open one by one, all the audio files and their TextGrids in the specified directory
# If the TextGrid does not exist, then create a new one
#
# Written by Rolando Muñoz A. (24 Sep 2017)
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# A copy of the GNU General Public License is available at
# <http://www.gnu.org/licenses/>.
#
include ../procedures/config.proc

@config.init: "../preferences.txt"

beginPause: "Audio transcriber"
  comment: "The directories where your files are stored..."
  sentence: "Audio folder", config.init.return$["audio_dir"]
  sentence: "Textgrid folder", config.init.return$["textgrid_dir"]
  comment: "If a TextGrid is not found, then create it..."
  sentence: "All tier names", config.init.return$["all_tier_names"]
  sentence: "Which of these are point tiers", config.init.return$["point_tiers"]
  comment: "Audio settings..."
  word: "Audio extension", config.init.return$["audio_extension"]
  boolean: "Adjust volume", 1
  comment: "TextGridEditor settings..."
  boolean: "Default values", 1
clicked = endPause: "Continue", "Quit", 1

if clicked = 2
  exitScript()
endif

# remember option values
@config.setField: "audio_dir", audio_folder$
@config.setField: "textgrid_dir", textgrid_folder$
@config.setField: "audio_extension", audio_extension$
@config.setField: "all_tier_names", all_tier_names$
@config.setField: "point_tiers", which_of_these_are_point_tiers$

# Read default values for TextGridEditor
min_range = number(config.init.return$["spectrogram.min_range"])
max_range = number(config.init.return$["spectrogram.max_range"])
dynamic_range = number(config.init.return$["spectrogram.dynamic_range"])
view_lenght = number(config.init.return$["spectrogram.view_lenght"])
show_pitch = number(config.init.return$["analysis.pitch"])
show_intensity = number(config.init.return$["analysis.intensity"])
show_formants = number(config.init.return$["analysis.formants"])
show_pulse = number(config.init.return$["analysis.pulse"])

# Check the dialog box
if all_tier_names$ == ""
  writeInfoLine: "Please, complete the ""All tier names"" field"
  exitScript()
audio_folder$ == ""
  writeInfoLine: "Please, complete the ""Audio folder"" field"
  exitScript()
elsif textgrid_folder$ == ""
  writeInfoLine: "Please, complete the ""TextGrid folder"" field"
  exitScript()
endif

audio_files = Create Strings as file list: "fileList", audio_folder$ + "/*.wav"
number_of_files = Get number of strings
if !number_of_files
  writeInfoLine: "No files in the Audio folder"
  exitScript()
endif

file_number = 1
volume = 0.99

while 1
  file_number = if file_number > number_of_files then 1 else file_number fi
  audio_name$ = object$[audio_files, file_number]
  textgrid_name$ = audio_name$ - audio_extension$ + ".TextGrid"

  # Open a Sound file from the list
  if adjust_volume
    sd = Read from file: audio_folder$ + "/"+ audio_name$
    Scale peak: volume
  else
    sd = Open long sound file: audio_folder$ + "/"+ audio_name$
  endif
  
  # If the corresponding textgrid exists, then open it. Otherwise, create it
  if fileReadable(textgrid_folder$ + "/"+ textgrid_name$)
    # Open the TextGrid
    tg = Read from file: textgrid_folder$ + "/"+ textgrid_name$
    status$ = "Exist"
  else
    # Create a TextGrid
    tg = To TextGrid: all_tier_names$, which_of_these_are_point_tiers$
    status$ = "New"
  endif
  
  selectObject: sd
  plusObject: tg
  View & Edit
  
  editor: tg
  if default_values
    Spectrogram settings: min_range, max_range, view_lenght, dynamic_range
    Show analyses: "yes", show_pitch, show_intensity, show_formants, show_pulse, 10
  endif
  
  beginPause: "Audio transcriber"
    comment: "Status: 'status$'"
    comment: "Case: 'file_number'/'number_of_files'"
    natural: "Next file",  if (file_number + 1) > number_of_files then 1 else file_number + 1 fi
    if adjust_volume
      real: "Volume", volume
    endif
  clicked = endPause: "Save", "Jump", "Quit", 1
  endeditor

  endeditor
  if clicked = 1
    selectObject: tg
    Save as text file: textgrid_folder$ + "/"+ textgrid_name$
  endif

  removeObject: sd, tg
  file_number = next_file

  if clicked = 3
    removeObject: audio_files
    exitScript()
  endif
endwhile
removeObject: audio_files
