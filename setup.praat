# Copyright 2017 Rolando Muñoz Aramburú

## Static menu
Add menu command: "Objects", "Goodies", "Audio transcriber", "", 0, ""
Add menu command: "Objects", "Goodies", "Run transcriber...", "Audio transcriber", 1, "scripts/open_audio_with_textgrids.praat"
Add menu command: "Objects", "Goodies", "Run transcriber (no annotated files)...", "Audio transcriber", 1, "scripts/open_sound_no_textgrid.praat"
Add menu command: "Objects", "Goodies", "Advanced settings", "Audio transcriber", 1, ""
Add menu command: "Objects", "Goodies", "TextGridEditor...", "Advanced settings", 2, "scripts/textgrid_editor_settings.praat"
Add menu command: "Objects", "Goodies", "-", "", 1, ""
Add menu command: "Objects", "Goodies", "About", "Audio transcriber", 1, "scripts/about.praat"