audio_dir: /home/rolando/gdrive/proyectos/urarina/Fieldwork (14)/2018/data/data_raw_v2/questionnaire_01/questionnaire_01_v4/assistant_08
textgrid_dir: /home/rolando/gdrive/proyectos/urarina/Fieldwork (14)/2018/data/data_raw_v2/questionnaire_01/questionnaire_01_v4/assistant_08
audio_extension: .wav
all_tier_names: tone seg word trans
point_tiers: tone
spectrogram.min_range: 0
spectrogram.max_range: 5000
spectrogram.dynamic_range: 60
spectrogram.view_lenght: 0.005
analysis.pitch: 1
analysis.intensity: 0
analysis.formants: 0
analysis.pulse: 0
number_file: 1
